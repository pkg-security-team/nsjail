Source: nsjail
Maintainer: Christian Blichmann <mail@blichmann.eu>
Section: admin
Priority: optional
Build-Depends: debhelper (>= 13~),
               bison,
               flex,
               libnl-route-3-dev,
               libprotobuf-dev,
               pkg-config,
               protobuf-compiler
Standards-Version: 4.5.0
Homepage: https://github.com/google/nsjail

Package: nsjail
Architecture: amd64 i386 arm64 arm mips64 mips
Built-Using: ${misc:Built-Using}
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Light-weight process isolation using namespaces and seccomp-bpf
 NsJail is a process isolation tool for Linux. It utilizes the Linux
 namespace subsystem, resource limits, and the seccomp-bpf syscall
 filters of the Linux kernel.
 .
 It can help you with (among other things):
  - Isolating networking services (e.g. web, time, DNS), by isolating
    them from the rest of the OS
  - Hosting computer security challenges (so-called CTFs)
  - Containing invasive syscall-level OS fuzzers
